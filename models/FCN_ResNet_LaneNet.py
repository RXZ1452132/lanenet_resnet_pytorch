import torch
from torchvision import models
from torchvision.models.resnet import ResNet, BasicBlock, Bottleneck
from torch import nn
from torch.nn import init
import numpy as np
import pdb
import torch.nn.functional as F
from scipy.linalg import sqrtm

# Returns 2D convolutional layer with space-preserving padding
def conv(in_planes, out_planes, kernel_size=3, stride=1, dilation=1, bias=False, transposed=False):
    if transposed:
        layer = nn.ConvTranspose2d(in_planes, out_planes, kernel_size=kernel_size, stride=stride, padding=1, output_padding=1, dilation=dilation, bias=bias)
        # Bilinear interpolation init
        w = torch.Tensor(kernel_size, kernel_size)
        centre = kernel_size % 2 == 1 and stride - 1 or stride - 0.5
        for y in range(kernel_size):
            for x in range(kernel_size):
                w[y, x] = (1 - abs((x - centre) / stride)) * (1 - abs((y - centre) / stride))
        layer.weight.data.copy_(w.div(in_planes).repeat(in_planes, out_planes, 1, 1))
    else:
        padding = (kernel_size + 2 * (dilation - 1)) // 2
        layer = nn.Conv2d(in_planes, out_planes, kernel_size=kernel_size, stride=stride, padding=padding, dilation=dilation, bias=bias)
    if bias:
        init.constant(layer.bias, 0)
    return layer


# Returns 2D batch normalisation layer
def bn(planes):
    layer = nn.BatchNorm2d(planes)
    # Use mean 0, standard deviation 1 init
    init.constant_(layer.weight, 1)
    init.constant_(layer.bias, 0)
    return layer



class FeatureResNet(ResNet):
    def __init__(self):
        super().__init__(BasicBlock, [2,2,2,2], 1000)
        # super().__init__(BasicBlock, [3,4,6,3], 1000)
        # super().__init__(Bottleneck, [3,4,6,3], 1000)

    def forward(self, x):
        x1 = self.conv1(x)
        x = self.bn1(x1)
        x = self.relu(x)
        x2 = self.maxpool(x)
        x = self.layer1(x2)
        x3 = self.layer2(x)
        x4 = self.layer3(x3)
        x5 = self.layer4(x4)
        return x1, x2, x3, x4, x5



class FCN8s_ResNet(nn.Module):
    def __init__(self, num_classes, pretrained=True):
        super().__init__()
        resnet = models.resnet18(pretrained)
        # resnet = models.resnet34(pretrained)
        # resnet = models.resnet50(pretrained)
        self.feature_resnet = FeatureResNet()
        self.feature_resnet.load_state_dict(resnet.state_dict())
        #Resnet34,18
        self.relu = nn.ReLU(inplace=True)
        self.conv5 = conv(512, 256, stride=2, transposed=True)
        self.bn5 = bn(256)
        self.conv6 = conv(256, 128, stride=2, transposed=True)
        self.bn6 = bn(128)
        self.conv7 = conv(128, 64, stride=2, transposed=True)
        self.bn7 = bn(64)
        self.conv8 = conv(64, 64, stride=2, transposed=True)
        self.bn8 = bn(64)
        self.conv9 = conv(64, 32, stride=2, transposed=True)
        self.bn9 = bn(32)
        self.deconv_final = conv(32, 32, kernel_size=7)
        self.bn_deconv_final = bn(32)
        self.logits_final = conv(32, num_classes, kernel_size=1)
        self.embeeding_conv = conv(32, 4, kernel_size=1)
        init.constant_(self.logits_final.weight, 0)  # Zero init

    def forward(self, x):
        x1, x2, x3, x4, x5 = self.feature_resnet(x)
        x4_ = self.relu(self.bn5(self.conv5(x5)))
        x4 = x4 + x4_
        x3_ = self.relu(self.bn6(self.conv6(x4)))
        x3 = x3 + x3_
        x2_ = self.relu(self.bn7(self.conv7(x3)))
        x2 = x2 + x2_
        x1_ = self.relu(self.bn8(self.conv8(x2)))
        x1 = x1 + x1_
        x1 = self.relu(self.bn9(self.conv9(x1)))
        deconv_final = self.relu(self.bn_deconv_final(self.deconv_final(x1)))
        binary_seg_logits = self.logits_final(deconv_final)
        binary_seg_embeeding = self.relu(self.embeeding_conv(deconv_final))
        binary_seg_ret = torch.argmax(F.softmax(binary_seg_logits, dim=1), dim=1, keepdim=True)
        ret = {
            'instance_seg_logits': binary_seg_embeeding,
            'binary_seg_pred': binary_seg_ret,
            'binary_seg_logits': binary_seg_logits
        }

        return ret

    def discriminative_loss(self, embedding, seg_gt, embed_dim=4, delta_v=0.5, delta_d=3.0):
        batch_size = embedding.shape[0]

        var_loss = torch.tensor(0, dtype=embedding.dtype, device=embedding.device)
        dist_loss = torch.tensor(0, dtype=embedding.dtype, device=embedding.device)
        reg_loss = torch.tensor(0, dtype=embedding.dtype, device=embedding.device)

        for b in range(batch_size):
            embedding_b = embedding[b]  # (embed_dim, H, W)
            seg_gt_b = seg_gt[b]

            labels = torch.unique(seg_gt_b)
            labels = labels[labels != 0]
            num_lanes = len(labels)
            if num_lanes == 0:
                # please refer to issue here: https://github.com/harryhan618/LaneNet/issues/12
                _nonsense = embedding.sum()
                _zero = torch.zeros_like(_nonsense)
                var_loss = var_loss + _nonsense * _zero
                dist_loss = dist_loss + _nonsense * _zero
                reg_loss = reg_loss + _nonsense * _zero
                continue

            centroid_mean = []
            for lane_idx in labels:
                seg_mask_i = (seg_gt_b == lane_idx)
                if not seg_mask_i.any():
                    continue
                embedding_i = embedding_b[:, seg_mask_i]

                mean_i = torch.mean(embedding_i, dim=1)
                centroid_mean.append(mean_i)

                # ---------- var_loss -------------
                var_loss = var_loss + torch.mean(
                    F.relu(torch.norm(embedding_i - mean_i.reshape(embed_dim, 1), dim=0) - delta_v) ** 2) / num_lanes
            centroid_mean = torch.stack(centroid_mean)  # (n_lane, embed_dim)

            if num_lanes > 1:
                centroid_mean1 = centroid_mean.reshape(-1, 1, embed_dim)
                centroid_mean2 = centroid_mean.reshape(1, -1, embed_dim)
                dist = torch.norm(centroid_mean1 - centroid_mean2, dim=2)  # shape (num_lanes, num_lanes)
                dist = dist + torch.eye(num_lanes, dtype=dist.dtype,
                                        device=dist.device) * delta_d  # diagonal elements are 0, now mask above delta_d

                # divided by two for double calculated loss above, for implementation convenience
                dist_loss = dist_loss + torch.sum(F.relu(-dist + delta_d) ** 2) / (num_lanes * (num_lanes - 1)) / 2

            # reg_loss is not used in original paper
            # reg_loss = reg_loss + torch.mean(torch.norm(centroid_mean, dim=1))

        var_loss = var_loss / batch_size
        dist_loss = dist_loss / batch_size
        reg_loss = reg_loss / batch_size
        return var_loss, dist_loss, reg_loss

    def compute_loss(self, inference_ret, binary_label, instance_label):
        k_binary = 0.7
        k_instance = 0.3
        decode_logits = inference_ret['binary_seg_logits']
        binary_label_plain = binary_label.view(binary_label.shape[0]*binary_label.shape[1]*binary_label.shape[2]).int()
        counts = torch.bincount(binary_label_plain)
        inverse_weights = 1.0/(torch.log((1.0/counts.float())+1.02))
        ce_loss_fn = nn.CrossEntropyLoss(weight=inverse_weights, reduction='mean')
        binary_loss = ce_loss_fn(decode_logits, binary_label.long())

        pix_embedding = inference_ret["instance_seg_logits"]
        # ds_loss_fn = DiscriminativeLoss(0.5, 1.5, 1.0, 1.0, 0.001)

        # instance_loss, _, _, _ = ds_loss_fn(pix_embedding, instance_label, [5] * len(pix_embedding))
        var_loss, dist_loss, reg_loss = self.discriminative_loss(pix_embedding, instance_label, embed_dim=4, delta_v=0.5, delta_d=3.0)
        instance_loss = 1.0*var_loss + 1.0*dist_loss + 0.001*reg_loss
        binary_loss = binary_loss * k_binary
        instance_loss = instance_loss * k_instance
        total_loss = binary_loss + instance_loss
        ret = {
                'total_loss': total_loss,
                'binary_seg_logits': decode_logits,
                'instance_seg_logits': pix_embedding,
                'binary_seg_loss': binary_loss,
                'discriminative_loss': instance_loss
            }

        return ret


