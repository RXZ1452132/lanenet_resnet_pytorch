"""
实现LaneNet的数据解析类
"""
import os.path as ops

import cv2
from torch.utils import data
import numpy as np
try:
    from cv2 import cv2
except ImportError:
    pass

class Tusimple(data.Dataset):

    def __init__(self, dataset_info_file, transform):
        """

        :param dataset_info_file:
        """
        self._gt_img_list, self._gt_label_binary_list, \
        self._gt_label_instance_list = self._init_dataset(dataset_info_file)
        self._next_batch_loop_count = 0
        self.transform = transform


    def _init_dataset(self, dataset_info_file):
        """

        :param dataset_info_file:
        :return:
        """
        gt_img_list = []
        gt_label_binary_list = []
        gt_label_instance_list = []

        assert ops.exists(dataset_info_file), '{:s}　不存在'.format(dataset_info_file)

        with open(dataset_info_file, 'r') as file:
            for _info in file:
                info_tmp = _info.strip(' ').split()

                gt_img_list.append(info_tmp[0])
                gt_label_binary_list.append(info_tmp[1])
                gt_label_instance_list.append(info_tmp[2])

        return gt_img_list[:], gt_label_binary_list[:], gt_label_instance_list[:]

    def __len__(self):

        return len(self._gt_img_list)


    def __getitem__(self, item):
        """

                :param batch_size:
                :return:
                """
        assert len(self._gt_label_binary_list) == len(self._gt_label_instance_list) \
               == len(self._gt_img_list)

        gt_img_path = self._gt_img_list[item]
        gt_label_binary_path = self._gt_label_binary_list[item]
        gt_label_instance_path = self._gt_label_instance_list[item]
        gt_img = cv2.imread(gt_img_path, cv2.IMREAD_COLOR)
        label_img = cv2.imread(gt_label_binary_path, cv2.IMREAD_COLOR)
        label_binary = np.zeros([label_img.shape[0], label_img.shape[1]], dtype=np.uint8)
        idx = np.where((label_img[:, :, :] != [0, 0, 0]).all(axis=2))
        label_binary[idx] = 1
        gt_label_binary = label_binary
        gt_label_instance = cv2.imread(gt_label_instance_path, cv2.IMREAD_UNCHANGED)

        sample = {'img': gt_img,
                  'binaryLabel': gt_label_binary,
                  'instanceLabel': gt_label_instance}
        if self.transform is not None:
            sample = self.transform(sample)

        paths = [gt_img_path, gt_label_binary_path, gt_label_instance_path]

        return sample['img'], sample['binaryLabel'], sample['instanceLabel'], paths
