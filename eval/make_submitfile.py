import pdb
import torch
import numpy as np
from models.FCN_myRes_Tusimple import Ipm_FCN8s_ResNet
from utils.transforms import Compose, Resize, Rotation, ToTensor, Normalize
import os.path as ops
from eval.eval_Tusimple_dataloader import Tusimple
from torch.utils.data import DataLoader
from torch.autograd import Variable
from eval.lanenet_postprocess import LaneNetPoseProcessor
from eval.lanenet_cluster import LaneNetCluster
import cv2
import argparse
import time
import json
import os


num_classes = 2

def validate(val_loader, net, train_args, submit_file):
    net.eval()

    for vi, data in enumerate(val_loader):
        with torch.no_grad():
            gt_img, paths = data
            time1 = time.time()
            inputs = Variable(gt_img).cuda()

            inference_ret = net(inputs)

            pix_embedding = inference_ret['instance_seg_logits']
            pix_embedding = pix_embedding.data.cpu().numpy()

            pix_embedding = np.transpose(pix_embedding, (0, 2, 3, 1))

            binary_seg_pred = inference_ret["binary_seg_pred"]
            binary_seg_pred = binary_seg_pred.data.squeeze(1).cpu().numpy()

            ppr = LaneNetPoseProcessor()
            binary_seg_after_pos = np.array([ppr.postprocess(image=binary_map) for binary_map in binary_seg_pred])

            cluster = LaneNetCluster()
            for ii in range(binary_seg_after_pos.shape[0]):
                sub_dict = {"h_samples":[160,170,180,190,200,210,220,230,240,250,260,270,280,290,300,310,320,330,340,350
                            ,360,370,380,390,400,410,420,430,440,450,460,470,480,490,500,510,520,530,540,550,560,570,580,
                            590,600,610,620,630,640,650,660,670,680,690,700,710],
                            "lanes":[],
                            "run_time":1000,
                            "raw_file":""}
                path = paths[ii]
                split_mask_img = cluster.get_split_lane_mask(binary_seg_after_pos[ii], pix_embedding[ii])
                path_spilt = path.split('+')[1:]

                sub_dict["raw_file"] = 'clips/' + '/'.join(path_spilt)
                h_samples = sub_dict["h_samples"]
                lanes = []
                for map in split_mask_img:
                    lane = []
                    map = cv2.resize(map, dsize=(1280, 720),interpolation=cv2.INTER_LINEAR)
                    for h in h_samples:
                        map_h = map[h, :]
                        xs = np.where(map_h==[255,255,255])
                        if len(xs[0])==0:
                            lane.append(-2)
                        else:
                            lane.append(np.mean(xs[0]))
                            # lane.append(int(np.mean(xs[0])))
                    lanes.append(lane)
                sub_dict["lanes"] = lanes
                time2 = time.time()
                print((time2 - time1)*1000)
                sub_dict["run_time"] = 100
                # time1 = time2
                #write json
                with open(submit_file, "a") as f:
                    json.dump(sub_dict, f)
                    f.writelines('\n')




def main(dataset_dir, submit_file):
    net = Ipm_FCN8s_ResNet(num_classes=num_classes, bev=args.bev).cuda()
    mean = (0.485, 0.456, 0.406)
    std = (0.229, 0.224, 0.225)
    resize_shape = (1280, 768)
    pretrained_model = torch.load(ops.join(args.weights_path))
    net.load_state_dict(pretrained_model)
    print('load pretrained model from {}'.format(args.weights_path))

    transform_val = Compose(Resize(resize_shape), ToTensor(),
                            Normalize(mean=mean, std=std))
    val_dataset_file = ops.join(dataset_dir, 'test.txt')


    valid_set = Tusimple(dataset_info_file=val_dataset_file, transform=transform_val)
    valid_loader = DataLoader(valid_set, batch_size=args.bs, num_workers=8, shuffle=False)

    if ops.exists(submit_file):
        os.remove(submit_file)
    validate(valid_loader, net, args, submit_file)


def init_args():
    """

    :return:
    """

    parser = argparse.ArgumentParser()

    parser.add_argument('--dataset_dir', type=str, default='/mnt/ExtraDisk/xiaozhou_temp/test_tusimple/testing/', help='The testing dataset dir path')
    parser.add_argument('--bs', type=int, default=1, help='The batch_size')
    parser.add_argument('--submit_file', type=str,
                        default='/mnt/ExtraDisk/xiaozhou_temp/test_tusimple/testing/submit.json', help='The target file')

    parser.add_argument('--print_freq', type=int, default=10, help='The ckpt')
    parser.add_argument('--bev', type=bool, default=False, help='with tf or not')
    parser.add_argument('--weights_path', type=str, default='/mnt/ExtraDisk/xiaozhou_temp/tusimple_lr4e-5_rr/fcn8s/epoch_144_loss_0.03087_mean-iu_0.41054_lr_0.0000400000.pth', help='The pretrained weights path')

    return parser.parse_args()

if __name__ == '__main__':
    args = init_args()
    main(args.dataset_dir, args.submit_file)
