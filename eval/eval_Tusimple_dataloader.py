"""
实现LaneNet的数据解析类
"""
import os.path as ops

import cv2
from torch.utils import data
import numpy as np
from PIL import Image
import torch
try:
    from cv2 import cv2
except ImportError:
    pass

class Tusimple(data.Dataset):

    def __init__(self, dataset_info_file, transform):
        """

        :param dataset_info_file:
        """
        self._gt_img_list = self._init_dataset(dataset_info_file)
        self._next_batch_loop_count = 0
        self.transform = transform


    def _init_dataset(self, dataset_info_file):
        """

        :param dataset_info_file:
        :return:
        """
        gt_img_list = []

        assert ops.exists(dataset_info_file), '{:s}　不存在'.format(dataset_info_file)

        with open(dataset_info_file, 'r') as file:
            for _info in file:
                info_tmp = _info.strip(' ').split()
                gt_img_list.append(info_tmp[0])

        return gt_img_list[:]

    def __len__(self):

        return len(self._gt_img_list)

    def __getitem__(self, item):
        """

                :param batch_size:
                :return:
                """
        gt_img_path = self._gt_img_list[item]

        gt_img = cv2.imread(gt_img_path, cv2.IMREAD_COLOR)


        sample = {'img': gt_img}
        if self.transform is not None:
            sample = self.transform(sample)

        return sample['img'], gt_img_path
