def train(train_loader, net, optimizer, epoch, train_args):
    train_loss = AverageMeter()
    curr_iter = (epoch - 1) * len(train_loader)
    # pdb.set_trace()
    time1 = time.time()
    for i, data in enumerate(train_loader):

        gt_img, label_binary, gt_labe_instance, _ = data

        N = gt_img.size(0)
        inputs = Variable(gt_img).cuda()
        label_binary = Variable(label_binary).cuda()
        gt_labe_instance = Variable(gt_labe_instance).cuda()

        optimizer.zero_grad()
        inference_ret = net(inputs)

        compute_ret = net.compute_loss(inference_ret=inference_ret, binary_label=label_binary,
                                       instance_label=gt_labe_instance)
        pdb.set_trace()
        total_loss = compute_ret['total_loss']
        binary_seg_loss = compute_ret['binary_seg_loss']
        disc_loss = compute_ret['discriminative_loss']
        pix_embedding = compute_ret['instance_seg_logits']

        binary_seg_pred = inference_ret["binary_seg_pred"]

        # iou = 0
        # batch_size = out.size()[0]
        # for i in range(batch_size):
        #     PR = out[i].squeeze(0).nonzero().size()[0]
        #     GT = label_binary[i].nonzero().size()[0]
        #     TP = (out[i].squeeze(0) * label_binary[i]).nonzero().size()[0]
        #     union = PR + GT - TP
        #     iou += TP / union
        # iou = iou / batch_size

        # print('total_loss', total_loss)
        total_loss.backward()
        optimizer.step()
        # pdb.set_trace()
        train_loss.update(total_loss.data.item(), N)

        curr_iter += 1
        writer.add_scalar('train_loss', total_loss.data.item(), curr_iter)
        if (i + 1) % train_args.print_freq == 0:
            time2 = time.time()
            time_delta = time2 - time1
            time1 = time2
            print('[epoch %d], [iter %d / %d], [train loss %.5f], [avg_train loss %.5f], [time %.3f s]' % (
                epoch, i + 1, len(train_loader), total_loss.data.item(), train_loss.avg, time_delta))
            # print(H_ouot)
            # print(loss.data.item())
            # print(loss2.data.item())

        if (i+1) % 2000 == 0:
            snapshot_name = 'train_epoch_%d_iter_%d_loss_%.5f_lr_%.10f' % (
                epoch, i + 1, train_loss.avg, optimizer.param_groups[1]['lr']
            )
            torch.save(net.state_dict(), os.path.join(args.ckpt_path, args.exp_name, snapshot_name + '.pth'))
            torch.save(optimizer.state_dict(), os.path.join(args.ckpt_path, args.exp_name, 'opt_' + snapshot_name + '.pth'))
            print('saving a new model')
