# Lanenet_ResNet_pytorch

Pytorch version of Lanenet, based on FCN & ResNet

## Install
```
pip3 install -r requirements.txt
```

## Make dataset(一次运行生成全部train_set, 根据自己的需要分割出一部分作为valid，注意图像数据和.txt文件对应)：

```angular2html
python make_dataset/generate_tusimple_dataset.py --src_dir path/to/your/unzipped/file
```

## Training：
(for example)
```
python train/train_ipm_Tusimple.py --train_dataset_dir /mnt/ExtraDisk/Tusiample_to_train/train_set/training/ --valid_dataset_dir /mnt/ExtraDisk/Tusiample_to_train/test_set/validing/ --ckpt_path /mnt/ExtraDisk/xiaozhou_temp/LaneNet_Torch/
```
If everything right, you will see:
![training](./training.png)

## Todo List

* [ ] Provid evaluate and test tools chain.  
* [ ] Provide pretrained model.
* [ ] Write H-Net codes.


## Reference
https://github.com/MaybeShewill-CV/lanenet-lane-detection

https://github.com/harryhan618/LaneNet

https://github.com/klintan/pytorch-lanenet