import os
from torch.backends import cudnn
from tensorboardX import SummaryWriter
import torch
import datetime
import random
import numpy as np
from torch import optim
from torch.autograd import Variable
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.utils.data import DataLoader
import torchvision.utils as vutils
import time
from dataloader import Tusimple
from models.FCN_ResNet_LaneNet import FCN8s_ResNet
from utils.transforms import Compose, Resize, Rotation, ToTensor, Normalize
from utils import check_mkdir, AverageMeter
import pdb
import os.path as ops
import argparse

cudnn.benchmark = True

num_classes = 2
best_record = {}

def main(args):

    net = FCN8s_ResNet(num_classes=num_classes).cuda()
    global best_record

    if len(args.snapshot) == 0:
        curr_epoch = 1
        best_record = {'epoch': 0, 'val_loss': 1e12,  'mean_iu': 0}

    else:
        print('training resumes from' + args.snapshot)
        pretrained_model = torch.load(os.path.join(args.ckpt_path, args.exp_name, args.snapshot))
        net.load_state_dict(pretrained_model)
        split_snapshot = args.snapshot.split('_')
        curr_epoch = int(split_snapshot[1]) + 1
        best_record = {'epoch': int(split_snapshot[1]), 'val_loss': float(split_snapshot[3]),
                               'mean_iu': float(split_snapshot[5])}

    net.train()
    mean = (0.485, 0.456, 0.406)
    std = (0.229, 0.224, 0.225)
    resize_shape = (1280, 768)
    transform_train = Compose(Resize(resize_shape), Rotation(2),
                              ToTensor(), Normalize(mean=mean, std=std))
    transform_val = Compose(Resize(resize_shape), ToTensor(),
                            Normalize(mean=mean, std=std))


    train_dataset_file = ops.join(args.train_dataset_dir, 'train.txt')

    val_dataset_file = ops.join(args.valid_dataset_dir, 'valid.txt')
    train_set = Tusimple(dataset_info_file=train_dataset_file, transform=transform_train)
    train_loader = DataLoader(train_set, batch_size=args.bs, num_workers=8, shuffle=True)
    valid_set = Tusimple(dataset_info_file=val_dataset_file, transform=transform_val)
    valid_loader = DataLoader(valid_set, batch_size=args.bs, num_workers=8, shuffle=False)


    optimizer = optim.Adam([
        {'params': [param for name, param in net.named_parameters() if name[-4:] == 'bias'],
         'lr': 2 * args.lr},
        {'params': [param for name, param in net.named_parameters() if name[-4:] != 'bias'],
         'lr': args.lr, 'weight_decay': args.weight_decay}
    ])#, momentum=args['momentum']


    if len(args.snapshot) > 0:
        optimizer.load_state_dict(torch.load(os.path.join(args.ckpt_path, args.exp_name, 'opt_' + args.snapshot)))
        optimizer.param_groups[0]['lr'] = 2 * args.lr
        optimizer.param_groups[1]['lr'] = args.lr

    check_mkdir(args.ckpt_path)
    check_mkdir(os.path.join(args.ckpt_path, args.exp_name))
    open(os.path.join(args.ckpt_path, args.exp_name, str(datetime.datetime.now()) + '.txt'), 'w').write(str(args) + '\n\n')

    scheduler = ReduceLROnPlateau(optimizer, 'min', patience=args.lr_patience, min_lr=1e-14)
    for epoch in range(curr_epoch, args.epoch_num + 1):
        train(train_loader, net, optimizer, epoch, args)
        val_loss = validate(valid_loader, net, optimizer, epoch, args)
        scheduler.step(val_loss)


def train(train_loader, net, optimizer, epoch, train_args):
    train_loss = AverageMeter()
    curr_iter = (epoch - 1) * len(train_loader)
    time1 = time.time()
    for i, data in enumerate(train_loader):

        gt_img, label_binary, gt_labe_instance, _ = data

        N = gt_img.size(0)
        inputs = Variable(gt_img).cuda()
        label_binary = Variable(label_binary).cuda()
        gt_labe_instance = Variable(gt_labe_instance).cuda()

        optimizer.zero_grad()
        inference_ret = net(inputs)

        compute_ret = net.compute_loss(inference_ret=inference_ret, binary_label=label_binary,
                                       instance_label=gt_labe_instance)
        total_loss = compute_ret['total_loss']
        binary_seg_loss = compute_ret['binary_seg_loss']
        disc_loss = compute_ret['discriminative_loss']
        pix_embedding = compute_ret['instance_seg_logits']

        binary_seg_pred = inference_ret["binary_seg_pred"]

        total_loss.backward()
        optimizer.step()
        # pdb.set_trace()
        train_loss.update(total_loss.data.item(), N)

        curr_iter += 1
        writer.add_scalar('train_loss', total_loss.data.item(), curr_iter)
        if (i + 1) % train_args.print_freq == 0:
            time2 = time.time()
            time_delta = time2 - time1
            time1 = time2
            print('[epoch %d], [iter %d / %d], [train loss %.5f], [binary_seg_loss %.5f], [disc_loss %.5f], [avg_train loss %.5f], [time %.3f s]' % (
                epoch, i + 1, len(train_loader), total_loss.data.item(), binary_seg_loss.data.item(), disc_loss.data.item(), train_loss.avg, time_delta))

        if (i+1) % 2000 == 0:
            snapshot_name = 'train_epoch_%d_iter_%d_loss_%.5f_lr_%.10f' % (
                epoch, i + 1, train_loss.avg, optimizer.param_groups[1]['lr']
            )
            torch.save(net.state_dict(), os.path.join(args.ckpt_path, args.exp_name, snapshot_name + '.pth'))
            torch.save(optimizer.state_dict(), os.path.join(args.ckpt_path, args.exp_name, 'opt_' + snapshot_name + '.pth'))
            print('saving a new model')


def validate(val_loader, net, optimizer, epoch, train_args):
    net.eval()
    val_loss = AverageMeter()
    count = 0
    datai = []
    iou = 0

    for vi, data in enumerate(val_loader):
        with torch.no_grad():
            if random.random() > train_args.val_img_sample_rate:
                pass
            else:
                datai.append(data)
            gt_img, label_binary, gt_labe_instance, _ = data
            N = gt_img.size(0)
            inputs = Variable(gt_img).cuda()
            label_binary = Variable(label_binary).cuda()
            gt_labe_instance = Variable(gt_labe_instance).cuda()
            # gts = do_label_trans(gts)
            inference_ret = net(inputs)

            compute_ret = net.compute_loss(inference_ret=inference_ret, binary_label=label_binary,
                                           instance_label=gt_labe_instance)
            #todo: condirm the shape
            total_loss = compute_ret['total_loss']
            binary_seg_loss = compute_ret['binary_seg_loss']
            disc_loss = compute_ret['discriminative_loss']
            pix_embedding = compute_ret['instance_seg_logits']

            binary_seg_pred = inference_ret["binary_seg_pred"]

        val_loss.update(total_loss.data.item(), N)

        batch_size = binary_seg_pred.size()[0]
        for i in range(batch_size):
            PR = binary_seg_pred[i].squeeze(0).nonzero().size()[0]
            GT = label_binary[i].nonzero().size()[0]
            TP = (binary_seg_pred[i].squeeze(0) * label_binary[i]).nonzero().size()[0]
            union = PR + GT - TP
            iou += TP / union
            count += 1
        if (vi + 1) % train_args.print_freq == 0:
            print(
                '[epoch %d], [iter %d / %d], [valid loss %.5f], [binary_seg_loss %.5f], [disc_loss %.5f], [avg_train loss %.5f]' % (
                    epoch, vi + 1, len(val_loader), total_loss.data.item(), binary_seg_loss.data.item(),
                    disc_loss.data.item(), val_loss.avg))
    iou = iou / count
    print(
        '-----------------------------------------------------------------------------------------------------------')
    print('[epoch %d], [val loss %.5f] [iou %.5f]' %(epoch, val_loss.avg, iou))
    if iou > best_record['mean_iu']:
        best_record['val_loss'] = val_loss.avg
        best_record['epoch'] = epoch
        best_record['mean_iu'] = iou
        snapshot_name = 'epoch_%d_loss_%.5f_mean-iu_%.5f_lr_%.10f' % (
            epoch, val_loss.avg, iou, optimizer.param_groups[1]['lr']
        )
        torch.save(net.state_dict(), os.path.join(args.ckpt_path, args.exp_name, snapshot_name + '.pth'))
        torch.save(optimizer.state_dict(), os.path.join(args.ckpt_path, args.exp_name, 'opt_' + snapshot_name + '.pth'))
        val_visual = []
        for vii, dataii in enumerate(datai):
            with torch.no_grad():
                gt_img, label_binary, gt_labe_instance, paths = dataii
                N = inputs.size(0)
                inputs = Variable(gt_img).cuda()
                label_binary = Variable(label_binary).cuda()
                gt_labe_instance = Variable(gt_labe_instance).cuda()
                inference_ret = net(inputs)

                compute_ret = net.compute_loss(inference_ret=inference_ret, binary_label=label_binary,
                                               instance_label=gt_labe_instance)
                pix_embedding = compute_ret['instance_seg_logits']

                binary_seg_pred = inference_ret["binary_seg_pred"]

            binary_seg_pred = binary_seg_pred.data.squeeze(1).cpu().numpy()
            label_binary = label_binary.data.cpu().numpy()
            binary_seg_pred_show = np.ones((binary_seg_pred.shape[1], binary_seg_pred.shape[2], 3), dtype=np.uint8)

            binary_seg_pred_show[binary_seg_pred[0] == 1] = binary_seg_pred_show[binary_seg_pred[0] == 1] * 255
            label_binary_show = np.ones((label_binary.shape[1], label_binary.shape[2], 3), dtype=np.uint8)
            label_binary_show[label_binary[0] == 1] = label_binary_show[label_binary[0] == 1] * 255
            # pdb.set_trace()
            import cv2
            ori_img = cv2.imread(paths[0][0])
            ori_img = cv2.cvtColor(ori_img, cv2.COLOR_BGR2RGB)
            ori_img = cv2.resize(ori_img, (1280,768), interpolation=cv2.INTER_LINEAR)

            val_visual.extend([torch.from_numpy(np.transpose(ori_img, (2,0,1))),
                               torch.from_numpy(np.transpose(label_binary_show, (2,0,1))),
                               torch.from_numpy(np.transpose(binary_seg_pred_show, (2,0,1)))])
        if len(val_visual) > 0:
            val_visual = torch.stack(val_visual, 0)
            val_visual = vutils.make_grid(val_visual, nrow=3, padding=5)
            writer.add_image(snapshot_name, val_visual)


    print('best record: [val loss %.5f],  [mean_iu %.5f], [epoch %d]' % (
        best_record['val_loss'],
        best_record['mean_iu'],
        best_record['epoch']))

    print(
        '-----------------------------------------------------------------------------------------------------------')

    writer.add_scalar('val_loss', val_loss.avg, epoch)
    writer.add_scalar('mean_iu', iou, epoch)
    writer.add_scalar('lr', optimizer.param_groups[1]['lr'], epoch)

    net.train()
    return val_loss.avg


def init_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('--train_dataset_dir', type=str, default='', help='The training dataset dir path')
    parser.add_argument('--valid_dataset_dir', type=str, default='', help='The validing dataset dir path')
    parser.add_argument('--bs', type=int, default=3, help='The batch_size')
    parser.add_argument('--epoch_num', type=int, default=5000, help='The total epoch num')
    parser.add_argument('--lr', type=float, default=4e-5, help='The learning rate')
    parser.add_argument('--weight_decay', type=float, default=5e-4, help='The weight decay')
    parser.add_argument('--momentum', type=float, default=0.95, help='The learning momentum')
    parser.add_argument('--lr_patience', type=int, default=100, help='The lr decay policy')
    parser.add_argument('--ckpt_path', type=str, default='', help='directory of model & records for tensorboard file')
    parser.add_argument('--exp_name', type=str, default='fcn8s', help='directory of model file')

    parser.add_argument('--snapshot', type=str, default='', help='The path of the model to load ')
    parser.add_argument('--print_freq', type=int, default=10, help='print frequency')
    parser.add_argument('--val_img_sample_rate', type=float, default=0.02, help='The sample rate of visualize images on tensorboard')


    return parser.parse_args()

if __name__ == '__main__':
    args = init_args()
    writer = SummaryWriter(os.path.join(args.ckpt_path, 'event', args.exp_name))

    main(args)
