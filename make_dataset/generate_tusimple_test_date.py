#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
处理tusimple数据集脚本
"""
import argparse
import glob
import json
import os
import os.path as ops
import shutil

import cv2

def init_args():
    """

    :return:
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--src_dir', type=str, default='/mnt/ExtraDisk/xiaozhou_temp/test_tusimple',
                        help='The origin path of unzipped tusimple dataset')

    return parser.parse_args()


def process_json_file(json_file_path, src_dir, ori_dst_dir):
    """

    :param json_file_path:
    :param src_dir: 原始clips文件路径
    :param ori_dst_dir: rgb训练样本
    :return:
    """
    assert ops.exists(json_file_path), '{:s} not exist'.format(json_file_path)

    with open(json_file_path, 'r') as file:
        for line_index, line in enumerate(file):
            info_dict = json.loads(line)

            image_dir = ops.split(info_dict['raw_file'])[0]
            image_dir_split = image_dir.split('/')[0:]
            image_dir_split.append(ops.split(info_dict['raw_file'])[1])
            image_name = '+'.join(image_dir_split)
            image_path = ops.join(src_dir, info_dict['raw_file'])
            import pdb; pdb.set_trace()
            assert ops.exists(image_path), '{:s} not exist'.format(image_path)

            src_image = cv2.imread(image_path, cv2.IMREAD_COLOR)

            dst_rgb_image_path = ops.join(ori_dst_dir, image_name)

            cv2.imwrite(dst_rgb_image_path, src_image)

            print('Process {:s} success'.format(image_name))


def gen_test_sample(src_dir,  image_dir):
    """
    生成图像训练列表
    :param src_dir:
    :param b_gt_image_dir: 二值基准图
    :param i_gt_image_dir: 实例分割基准图
    :param image_dir: 原始图像
    :return:
    """

    with open('{:s}/testing/test.txt'.format(src_dir), 'w') as file:

        for image_name in os.listdir(image_dir):
            if not image_name.endswith('.jpg'):
                continue

            image_path = ops.join(image_dir, image_name)

            assert ops.exists(image_path), '{:s} not exist'.format(image_path)

            image = cv2.imread(image_path, cv2.IMREAD_COLOR)

            if image is None:
                print('图像对: {:s}损坏'.format(image_name))
                continue
            else:
                info = '{:s}'.format(image_path)
                file.write(info + '\n')
    return


def process_tusimple_dataset(src_dir):
    """

    :param src_dir:
    :return:
    """
    testing_folder_path = ops.join(src_dir, 'testing')
    os.makedirs(testing_folder_path, exist_ok=True)

    for json_label_path in glob.glob('{:s}/test_tasks_0627.json'.format(src_dir)):
        json_label_name = ops.split(json_label_path)[1]

        shutil.copyfile(json_label_path, ops.join(testing_folder_path, json_label_name))

    gt_image_dir = ops.join(testing_folder_path, 'gt_image')
    os.makedirs(gt_image_dir, exist_ok=True)

    for json_label_path in glob.glob('{:s}/*.json'.format(testing_folder_path)):
        process_json_file(json_label_path, src_dir, gt_image_dir)

    gen_test_sample(src_dir, gt_image_dir)

    return


if __name__ == '__main__':
    args = init_args()

    process_tusimple_dataset(args.src_dir)
